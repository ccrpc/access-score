# Tiles Export Script

This is a script which will run queries on a postgres database and then export the results as `.gpkg` files.

It will then also convert those `.gpkg` files to `.mbtiles`

## Setup

Create a copy of the `.env.example` file and name it `.env`

In this file, create environment variables to authenticate with the PostgreSQL you wich to query.

An example of the `.env` file is below: 
```sh
  POSTGRES_DB=pcd
  POSTGRES_HOST=gis.ccrpc.org
  POSTGRES_USER=pcd_admin
  POSTGRES_PASSWORD=<replace-me>
```

## Running the Script

This script is designed to run in a Docker/Podman container.

If you machine has docker, run the `run_docker.sh` script. If it instead has podman, run the `run_podman.sh` script.

These scripts are set up to build an image based on the contents of this folder, run the image setting up proper volume mounting and passing in the `.env` file, and then delete the container and image they create.

The resulting `.gpkg` and `.mbtiles` will be placed in the `output` folder.

## Structure of the Export Scripts

Each of the export_* scripts has three main parts.

### The SQL Queries
Each resulting `*.gpkg` file will consist of one or more layers.

Each layer has its own SQL query, passed in via the read sh syntax.
```sh
read -r -d '' service_boundary_query << EOM
SELECT ST_Union(geom) as geom FROM census.service_boundary_test  
EOM
```

### Adding the layers
Once all of your queries have been defined, each export script adds the layer to the resulting `.gpkg` file via the `add_layer` helper.
```sh
add_layer "zcta" "4" "13" "$census_zcta_query"
add_layer "centroid" "1" "13" "$zcta_centroid_query"
```

The `add_helper` layer accepts 4 arguments:
- NAME:    The start of the name of the layer.
  - If you later load the resulting file into QGIS, the layer name will be NAME_MINZOOM_MAXZOOM
- MINZOOM: The minimum zoom for this layer.
- MAXZOOM: The maximum zoom for this layer.
- QUERY:   The sql query to create this layer, most likely stored in a variable above.

### Tying it all together
After all of the layers have been defined and added, it is time to export the file. This is done via the `export_tiles` helper.
```sh
export_tiles \
    "Ameren Illinois Community Partner Data Portal Census Tiles" \
    "Census data for the Ameren Illinois Community Partner Data Portal" \
    "4" "13"
```

The `export_tiles` helper accepts 4 arguments:
- NAME:        The name the object inside the `gpkg` file will have
- DESCRIPTION: A description of the data
- MINZOOM:     The minimum zoom of the geopackage. Going beyond this zoom will likely be done with interpolation or just not display the data
- MAXZOOM:     The maximum zoom of the geopackage. Zooming in more than this will likely hide the data.