#!/bin/sh

echo "Exporting baseline"
./export_baseline.sh

echo "Exporting Business As Usual Scenario"
./export_bau.sh

echo "Exporting Business As Usual Difference"
./export_bau_difference.sh

echo "Exporting Optimisitic Scenario"
./export_optimistic.sh

echo "Exporting Optimisitic Difference"
./export_optimistic_difference.sh

echo "Exporting Transformative Scenario"
./export_transformative.sh

echo "Exporting Transformative Difference"
./export_transformative_difference.sh

echo "Done Exporting Tiles"
