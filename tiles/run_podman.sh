#!/bin/bash

podman build -t access-score.tiles:export .
podman run -v $PWD/output:/data:Z --env-file .env --name export_tiles access-score.tiles:export
podman container rm export_tiles
podman image rm access-score.tiles:export
