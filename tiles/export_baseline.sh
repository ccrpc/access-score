#!/bin/sh

# Data sources: PostGIS and GeoPackage
GPKG=/data/baseline-2020.gpkg
MBTILES=/data/baseline-2020.mbtiles

# Include helpers
source /tiles/tile_helpers.sh

read -r -d '' segment_query <<EOM
SELECT *
FROM access_score.access_score_baseline_2020
EOM

read -r -d '' destination_query <<EOM
SELECT *
FROM access_score.destinations_2050
EOM

remove_file "$GPKG"
remove_file "$MBTILES"
add_layer "segment" "0" "14" "$segment_query"
add_layer "destination" "12" "14" "$destination_query"

# Convert GeoPackage to MBTiles
export_tiles \
    "Baseline Accessibility in 2020" \
    "Accessibility scores from the Sustainable Neighborhoods analysis of Champaign County, Illinois" \
    "0" "14"
