export const config = {
  namespace: 'access-score',
  outputTargets: [
    {
      type: 'www',
      baseUrl: '/access-score',
      serviceWorker: null
    }
  ],
  plugins: [],
  globalScript: 'src/global/access-score.ts',
  globalStyle: 'src/global/access-score.css'
};
