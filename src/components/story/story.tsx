import { h, Component, Host, Element, Prop, Watch } from '@stencil/core';


@Component({
  styleUrl: 'story.css',
  tag: 'as-story'
})
export class Story {
  @Element() el: HTMLElement;

  @Prop() active: boolean = false;
  @Prop() name: string;

  @Watch('active')
  setStep(isActive: boolean) {
    let story = this.el.querySelector('gl-story');
    if (story) story.step = (isActive) ? 0 : undefined;
  }

  render() {
    return (
      <Host class="ion-padding"
        style={{
          display: (this.active) ? 'block' : 'none'
        }}>
        <slot />
      </Host>
    )
  }
}


