import { h, Component, Element, Host, Listen, State } from '@stencil/core';
import { _t } from '../../i18n/i18n';
import { colorRamps } from '../utils';


@Component({
  styleUrl: 'legend.css',
  tag: 'as-legend'
})
export class Legend {
  @Element() el: HTMLElement;
  @State() baseline: boolean = true;
  @State() compare: boolean = true;
  @State() drawerOpen: boolean = false;

  async componentWillLoad() {
    let scenarios = document.querySelector('as-scenarios');
    await scenarios.componentOnReady();
    this.togglechangeHandler();
  }

  @Listen('asToggleChange', { target: 'body' })
  @Listen('asScenarioChange', { target: 'body' })
  togglechangeHandler() {
    let scenarios = document.querySelector('as-scenarios');
    let scenario = scenarios.scenario;
    this.baseline = (scenario) ? !scenario.difference : true;
    this.compare = scenarios.compare;
  }

  @Listen('glDrawerToggle', {target: 'body'})
  setDrawerOpen(e: CustomEvent) {
    this.drawerOpen = e.detail.open;
  }

  render() {
    let scoreType = (!this.baseline && this.compare) ?
      'difference' : 'absolute';
    let ramp = colorRamps[scoreType];
    let scores = [];
    let colors = [];
    ramp.forEach((value: any, i: number) =>
      ((i % 2 == 0) ? scores : colors).push(value));
    let gradient = `linear-gradient(to left, ${colors.reverse().join(' ,')})`;
    let scoreLabels = scores.map((score) =>
      (scoreType === 'difference' && score > 0) ? `+${score}` : `${score}`);

    return (
      <Host class={(this.drawerOpen) ? 'drawer-open' : 'drawer-closed'}>
        <aside>
          <h2>{_t('as.legend.title')}</h2>
          <h3 class={scoreType}>{_t(`as.legend.${scoreType}`)}</h3>
          <div class="score-bar" style={{'background-image': gradient}}></div>
          <div class="score-labels">{scoreLabels.join(' ')}</div>
          {(!this.baseline) ?
            <h3 class="project">{_t('as.legend.project')}</h3> : null}
        </aside>
      </Host>
    );
  }
}
