import { h, Component, Prop } from '@stencil/core';


@Component({
  styleUrl: 'score.css',
  tag: 'as-score'
})
export class Score {
  @Prop() compare: boolean = false;
  @Prop() icon: string;
  @Prop() label: string;
  @Prop() value: number;
  @Prop() color: string = '#dddddd';

  render() {
    let value = (this.value > 0 && this.compare) ?
      `+${this.value}` : `${this.value}`;
    return (
      <ion-item>
        { (this.icon) ?
          <ion-icon name={this.icon} slot="start"></ion-icon> : null }
        <ion-label>{this.label}</ion-label>
        {(this.value !== null) ? <span class="score"
            style={{borderColor: this.color}}
          >{value}</span> : null}
      </ion-item>
    );
  }
}
