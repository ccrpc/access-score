import { h, Component, } from '@stencil/core';
import { popoverController } from '@ionic/core';
import { _t } from '../../i18n/i18n';


@Component({
  tag: 'as-info-button'
})
export class InfoButton {

  async openPopover(e: Event) {
    const popover = await popoverController.create({
      component: 'as-info-menu',
      event: e
    });

    await popover.present();
  }

  render() {
    return (
      <ion-button onClick={(e) => this.openPopover(e)}
          title={_t('as.info-button.title')}>
        <ion-icon slot="icon-only" name="information-circle"></ion-icon>
      </ion-button>
    );
  }
}


