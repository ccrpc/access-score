import { h, Component, Element, Prop } from '@stencil/core';
import { _t } from '../../i18n/i18n';


@Component({
  tag: 'as-scenario'
})
export class Scenario {
  @Element() el: HTMLElement;
  @Prop() difference: string;

  render() {
    return (
      <ion-select-option value={this.el.id}>
        {_t(`as.scenario.${this.el.id}`)}
      </ion-select-option>
    );
  }
}
